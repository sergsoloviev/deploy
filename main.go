package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
)

var (
	conf Configuration
)

type Configuration struct {
	Listen struct {
		Ip   string `json: "ip"`
		Port string `json: "port"`
	}
	Log      string `json: "log"`
	Projects map[string]string
}

func catch(err error) {
	if err != nil {
		panic(err)
	}
}

func configure(file_name string) Configuration {
	bytes, err := ioutil.ReadFile(file_name)
	catch(err)
	err = json.Unmarshal(bytes, &conf)
	catch(err)
	return conf
}

func main() {
	conf := configure("conf/config.json")
	logfile, err := os.OpenFile(conf.Log, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	catch(err)
	defer logfile.Close()
	log.SetOutput(logfile)

	bind := fmt.Sprintf("%s:%s", conf.Listen.Ip, conf.Listen.Port)
	log.Printf("Listening on %s...", bind)

	http.HandleFunc("/", handler)
	http.ListenAndServe(bind, nil)
}

func handler(w http.ResponseWriter, r *http.Request) {
	name := path.Base(r.URL.Path)
	script := conf.Projects[name]
	if len(script) == 0 {
		log.Printf("cant find configuration for %s", name)
		return
	}
	log.Printf("exec %s", script)

	go executer(script)
	fmt.Fprintf(w, "ok")
}

func executer(command string) {
	out, err := exec.Command("/bin/bash", command).Output()
	if err != nil {
		log.Printf("%v", err)
		return
	}
	log.Printf("%s", out)
}
