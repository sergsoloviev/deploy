upstream [UPSTREAM] {
    #server unix:/tmp/site.socket fail_timeout=0;
    server 127.0.0.1:9999 fail_timeout=0;
}
server {
    listen 80;
    server_name  [DOMAIN];
    proxy_set_header Host [DOMAIN];
    location / {
        rewrite ^(.*)$ https://[DOMAIN]$1 permanent;
    }
}
server {
    #listen       80;
    listen       443 ssl;
    server_name  [DOMAIN];
    charset      utf-8;

    allow [ALLOW_IP];
    deny  all;

    ssl_certificate /etc/letsencrypt/live/[DOMAIN]/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/[DOMAIN]/privkey.pem;
    ssl_session_cache shared:le_nginx_SSL:1m;
    ssl_session_timeout 1440m;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;

    ssl_ciphers "ECDHE-ECDSA-AES128-GCM-SHA256 ECDHE-ECDSA-AES256-GCM-SHA384 ECDHE-ECDSA-AES128-SHA ECDHE-ECDSA-AES256-SHA ECDHE-ECDSA-AES128-SHA256 ECDHE-ECDSA-AES256-SHA384 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES256-GCM-SHA384 ECDHE-RSA-AES128-SHA ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES256-SHA384 DHE-RSA-AES128-GCM-SHA256 DHE-RSA-AES256-GCM-SHA384 DHE-RSA-AES128-SHA DHE-RSA-AES256-SHA DHE-RSA-AES128-SHA256 DHE-RSA-AES256-SHA256 EDH-RSA-DES-CBC3-SHA";

    sendfile on;

    location / {
        proxy_pass http://[UPSTREAM];
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_connect_timeout 30s;
        proxy_read_timeout 30s;
        proxy_redirect off;
    }
    location ~ /\.ht {
        deny  all;
    }
    location /favicon.ico {
        return 204;
        access_log     off;
        log_not_found  off;
    }
    location /robots.txt {
        return 204;
        access_log     off;
        log_not_found  off;
    }
}